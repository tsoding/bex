# **B**ot **EX**pression (Bex)

A simple Interpreted Dynamic Programming Language for describing chat bot commands and behavior.

## Quick Start

Just copy-paste [./bex.go](./bex.go) into your project and start using
it. Check [./beme.go](./beme.go) for the usage example.

## Language Elements

### String

```
"Hello, World"
```

### Integer

```
69
420
```

### Variable

```
args
sender
```

### Funcalls

```
f()
f(g())
f(69, 420)
f("hello, world")
```
