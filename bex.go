// This is the main library. It's a single file and it's simple enough
// that you can simply copy paste it into your project.
//
// For a usage example check out ./beme.go
package main

import (
	"fmt"
	"errors"
	"unicode"
	"strings"
	"strconv"
)

type TokenType int

const (
	TokenInvalid TokenType = iota
	TokenSym
	TokenNum
	TokenStr
	TokenOParen
	TokenCParen
	TokenComma
)

var TokenTypeName = map[TokenType]string{
	TokenInvalid: "TokenInvalid",
	TokenSym: "TokenSym",
	TokenNum: "TokenNum",
	TokenStr: "TokenStr",
	TokenOParen: "TokenOParen",
	TokenCParen: "TokenCParen",
	TokenComma: "TokenComma",
}

type Loc struct {
	FilePath string
	Row int
	Col int
}

func (l Loc) String() string {
	return fmt.Sprintf("%s:%d:%d", l.FilePath, l.Row, l.Col)
}

type Token struct {
	Type TokenType
	Text []rune
	Loc Loc
}

type Lexer struct {
	FilePath string
	Content []rune
	Cur int
	Bol int
	Row int
	PeekTok Token
	PeekErr error
	PeekFull bool
}

func NewLexer(content []rune, filePath string) Lexer {
	return Lexer{
		FilePath: filePath,
		Content: content,
	}
}

func (l *Lexer) ChopChar() {
	if l.Cur < len(l.Content) {
		x := l.Content[l.Cur]
		l.Cur += 1
		if x == '\n' {
			l.Bol = l.Cur
			l.Row += 1
		}
	}
}

func (l *Lexer) TrimLeft() {
	for l.Cur < len(l.Content) && unicode.IsSpace(l.Content[l.Cur]) {
		l.ChopChar()
	}
}

func (l *Lexer) StartsWith(prefix []rune) bool {
	if l.Cur + len(prefix) > len(l.Content) {
		return false
	}

	for i := 0; i < len(prefix); i += 1 {
		if prefix[i] != l.Content[l.Cur + i] {
			return false
		}
	}

	return true
}

func (l *Lexer) Loc() Loc {
	return Loc{
		FilePath: l.FilePath,
		Row: l.Row + 1,
		Col: l.Cur - l.Bol + 1,
	}
}

type DiagErr struct {
	Loc Loc
	Err error
}

func (e *DiagErr) Error() string {
	return fmt.Sprintf("%s: ERROR: %s", e.Loc, e.Err)
}

func (e *DiagErr) Unwrap() error {
	return e.Err
}

var (
	LexerEOF = errors.New("Lexer: End of file")
	LexerUnclosedStr = errors.New("Lexer: Unclosed String")
	LexerInvalidEscape = errors.New("Lexer: Invalid Escape Sequence")
	LexerInvalidToken = errors.New("Lexer: Invalid Token")
)

func (l *Lexer) ChopWhile(p func(rune) bool) (result []rune) {
	for l.Cur < len(l.Content) && p(l.Content[l.Cur]) {
		result = append(result, l.Content[l.Cur])
		l.ChopChar()
	}
	return
}

func (l *Lexer) ChopToken() (Token, error) {
	t := Token{}

	l.TrimLeft()
	for l.Cur < len(l.Content) && l.StartsWith([]rune("//")) {
		for l.Cur < len(l.Content) && l.Content[l.Cur] != '\n' {
			l.ChopChar()
		}
		l.TrimLeft()
	}

	t.Loc = l.Loc()

	if l.Cur >= len(l.Content) {
		return t, LexerEOF
	}

	first := l.Content[l.Cur]

	ps := []rune("(),")
	ts := []TokenType{TokenOParen, TokenCParen, TokenComma}
	for i := range(ps) {
		if first == ps[i] {
			t.Type = ts[i]
			t.Text = []rune{ps[i]}
			l.ChopChar()
			return t, nil
		}
	}

	if unicode.IsDigit(first) {
		t.Type = TokenNum;
		t.Text = l.ChopWhile(unicode.IsDigit);
		return t, nil
	}

	if unicode.IsLetter(first) || first == '_' {
		t.Type = TokenSym;
		t.Text = l.ChopWhile(func (x rune) bool {
			return unicode.IsLetter(x) || unicode.IsDigit(x) || x == '_';
		});
		return t, nil;
	}

	if first == '"' {
		l.ChopChar()

		t.Type = TokenStr;

		for l.Cur < len(l.Content) && l.Content[l.Cur] != '"' {
			if l.Content[l.Cur] == '\\' {
				l.ChopChar();
				if l.Cur >= len(l.Content) {
					return t, &DiagErr{
						Loc: l.Loc(),
						Err: LexerUnclosedStr,
					}
				}
				if l.Content[l.Cur] == '"' {
					t.Text = append(t.Text, '"');
					l.ChopChar()
				} else {
					loc := l.Loc()
					l.ChopChar()
					return t, &DiagErr{
						Loc: loc,
						Err: fmt.Errorf("%w: %c", LexerInvalidEscape, l.Content[l.Cur]),
					}
				}
			} else {
				t.Text = append(t.Text, l.Content[l.Cur]);
				l.ChopChar()
			}
		}

		if l.Cur >= len(l.Content) {
			return t, &DiagErr{
				Loc: l.Loc(),
				Err: LexerUnclosedStr,
			}
		}

		l.ChopChar()
		return t, nil;
	}

	l.ChopChar()
	return t, &DiagErr{
		Loc: l.Loc(),
		Err: fmt.Errorf("%w: %c", LexerInvalidToken, first),
	}
}

func (l *Lexer) Peek() (Token, error) {
	if !l.PeekFull {
		l.PeekTok, l.PeekErr = l.ChopToken()
		l.PeekFull = true
	}
	return l.PeekTok, l.PeekErr
}

func (l *Lexer) Next() (Token, error) {
	if l.PeekFull {
		l.PeekFull = false
	} else {
		l.PeekTok, l.PeekErr = l.ChopToken()
	}
	return l.PeekTok, l.PeekErr
}

type ExprType int

const (
	ExprVoid ExprType = iota
	ExprInt
	ExprStr
	ExprVar
	ExprFuncall
)

type Expr struct {
	Type ExprType
	Loc Loc
	AsInt int
	AsStr string
	AsVar string
	AsFuncall Funcall
}

type Funcall struct {
	Name string
	Args []Expr
}

func (expr *Expr) Dump(level int) {
	for i := 0; i < level; i += 1 {
		fmt.Printf("  ");
	}

	switch expr.Type {
	case ExprVoid:
		fmt.Printf("Void\n");
	case ExprInt:
		fmt.Printf("Int: %d\n", expr.AsInt);
	case ExprStr:
		// TODO: Expr.Dump() does not escape strings
		fmt.Printf("Str: \"%s\"\n", expr.AsStr);
	case ExprVar:
		fmt.Printf("Var: %s\n", expr.AsVar);
	case ExprFuncall:
		fmt.Printf("Funcall: %s\n", expr.AsFuncall.Name)
		for _, arg := range expr.AsFuncall.Args {
			arg.Dump(level + 1)
		}
	}
}

func (expr *Expr) String() string {
	switch expr.Type {
	case ExprVoid: return ""
	case ExprInt: return fmt.Sprintf("%d", expr.AsInt)
	// TODO: Expr.String() does not escape string
	case ExprStr: return fmt.Sprintf("\"%s\"", expr.AsStr)
	case ExprVar: return fmt.Sprintf("%s", expr.AsVar)
	case ExprFuncall: return expr.AsFuncall.String()
	}
	panic("unreachable")
}

func (funcall *Funcall) String() string {
	var result strings.Builder
	fmt.Fprintf(&result, "%s(", funcall.Name)
	for i, arg := range funcall.Args {
		if i > 0 {
			fmt.Fprintf(&result, ", ")
		}
		fmt.Fprintf(&result, "%s", arg.String())
	}
	fmt.Fprintf(&result, ")")
	return result.String()
}

var (
	ParserUnexpectedToken = errors.New("Parser: Unexpected Token")
	ParserUnclosedFuncall = errors.New("Parser: Unclosed Funcall")
)

func ParseExpr(l *Lexer) (Expr, error) {
	t, err := l.Next()
	if err != nil {
		return Expr{}, err
	}

	switch t.Type {
	case TokenSym:
		oparen, err := l.Peek()
		if err != nil || oparen.Type != TokenOParen {
			return Expr{
				Type: ExprVar,
				Loc: t.Loc,
				AsVar: string(t.Text),
			}, nil;
		} else {
			l.Next()
			cparen, err := l.Peek()

			if err == nil && cparen.Type == TokenCParen {
				l.Next()
				return Expr{
					Type: ExprFuncall,
					Loc: t.Loc,
					AsFuncall: Funcall{
						Name: string(t.Text),
					},
				}, nil
			}

			arg, err := ParseExpr(l)
			if err != nil {
				return Expr{}, err
			}

			args := []Expr{arg}

			comma, err := l.Next()
			for err == nil && comma.Type == TokenComma {
				arg, err = ParseExpr(l)
				if err != nil {
					return Expr{}, err
				}
				args = append(args, arg)
				comma, err = l.Next()
			}

			if err == LexerEOF || comma.Type != TokenCParen {
				return Expr{}, &DiagErr{
					Loc: comma.Loc,
					Err: ParserUnclosedFuncall,
				}
			}

			return Expr{
				Type: ExprFuncall,
				Loc: t.Loc,
				AsFuncall: Funcall{
					Name: string(t.Text),
					Args: args,
				},
			}, nil
		}
	case TokenNum:
		s := string(t.Text)
		x, err := strconv.Atoi(s)
		if err != nil {
			return Expr{}, &DiagErr{
				Loc: t.Loc,
				Err: err,
			}
		}
		return Expr{
			Type: ExprInt,
			Loc: t.Loc,
			AsInt: x,
		}, nil
	case TokenStr:
		return Expr{
			Type: ExprStr,
			Loc: t.Loc,
			AsStr: string(t.Text),
		}, nil
	}

	return Expr{}, &DiagErr{
		Loc: t.Loc,
		Err: fmt.Errorf("%w: %s", ParserUnexpectedToken, TokenTypeName[t.Type]),
	}
}

func ParseAllExprs(l *Lexer) ([]Expr, error) {
	exprs := []Expr{}
	for {
		expr, err := ParseExpr(l)
		if err != nil {
			if errors.Is(err, LexerEOF) {
				err = nil
			}
			return exprs, err
		}
		exprs = append(exprs, expr)
	}
}

type EvalScope struct {
	Vars map[string]Expr
	Funcs map[string]Func
}

type EvalContext struct {
	Scopes []EvalScope
}

func (context *EvalContext) LookUpVar(name string) (Expr, bool) {
	scopes := context.Scopes
	for len(scopes) > 0 {
		n := len(scopes)
		varr, ok := scopes[n-1].Vars[name]
		if ok {
			return varr, true
		}
		scopes = scopes[:n-1]
	}
	return Expr{}, false
}

func (context *EvalContext) LookUpFunc(name string) (Func, bool) {
	scopes := context.Scopes
	for len(scopes) > 0 {
		n := len(scopes)
		fun, ok := scopes[n-1].Funcs[name]
		if ok {
			return fun, true
		}
		scopes = scopes[:n-1]
	}
	return nil, false
}

func (context *EvalContext) PushScope(scope EvalScope) {
	context.Scopes = append(context.Scopes, scope)
}

func (context *EvalContext) PopScope() {
	n := len(context.Scopes)
	context.Scopes = context.Scopes[0:n-1]
}

func (context *EvalContext) TopScope() *EvalScope {
	n := len(context.Scopes)
	if n <= 0 {
		panic("No scopes found")
	}
	return &context.Scopes[n-1]
}

type Func = func(context *EvalContext, args []Expr) (Expr, error)

var (
	RuntimeUnknownVar = errors.New("Runtime: Unknown variable")
	RuntimeUnknownFun = errors.New("Runtime: Unknown function")
)

func (context *EvalContext) EvalExpr(expr Expr) (Expr, error) {
	switch expr.Type {
	case ExprVoid, ExprInt, ExprStr:
		return expr, nil
	case ExprVar:
		val, ok := context.LookUpVar(expr.AsVar)
		if !ok {
			return Expr{}, fmt.Errorf("%s: ERROR: %w: %s", expr.Loc, RuntimeUnknownVar, expr.AsVar)
		}
		return context.EvalExpr(val)
	case ExprFuncall:
		fun, ok := context.LookUpFunc(expr.AsFuncall.Name)
		if !ok {
			return Expr{}, fmt.Errorf("%s: ERROR: %w: %s", expr.Loc, RuntimeUnknownFun, expr.AsFuncall.Name)
		}
		return fun(context, expr.AsFuncall.Args)
	}
	panic("unreachable")
}
